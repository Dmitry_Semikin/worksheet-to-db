package xyz.dsemikin.worksheettodb;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class App {
    public static void main(String[] args) throws ExcelFileStructureException, IOException, SQLException {

        // TODO: Probably we need some library to parse arguments.
        // TODO: Optionally filter out existing records by append.

        if (args.length < 4 || args.length > 5) {
            throw new IllegalArgumentException("""
                    "Application takes exactly four arguments:
                     1. path to excel file,
                     2. DB connection string
                     3. DB username
                     4. DB password
                     5. --append (optional flag) - does not create tables. Inserts data to existing tables.
                    """);
        }

        final String inputPath = args[0];
        final String connectionString = args[1];
        final String dbUsername = args[2];
        final String dbPassword = args[3];


        final boolean append = args.length > 4 && args[4].equals("--append");

        final Path inputFilePath = Paths.get(inputPath);
        Map<String, List<Map<String, ExcelValueWrapper>>> data = ExcelFileReader.readExcelFile(inputFilePath);

        final Properties connectionProperties = new Properties();
        connectionProperties.put("user", dbUsername);
        connectionProperties.put("password", dbPassword);

        SqlDbTableImporter.importTables(connectionString, connectionProperties, data, append);

        System.out.println("Done.");
    }

}
